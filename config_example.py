GITLAB_URL = 'https://gitlab.com'
GITLAB_TOKEN = 'PUT_YOUR_TOKEN'

ERROR_CREATING_REPO = "Can't create repo :("
COMPLETE_MESSAGE = "Complete"

GITLAB_AUTHOR_EMAIL = "email"
GITLAB_AUTHOR_NAME = "gitlab_username"

GOOGLE_ACCOUNT_CREDENTIALS_FILE = ""  # Относительный (от корня репы) путь до файла с гугловским аккаунтом для апишки

MAX_RETRY = 10  # Максимальное количество ретраев на одного студента (при ошибке)

BROKEN_GROUPS = ['52', '51']  # Группы, которые начинаются с нуля (им надо 0 дописать в начале)
