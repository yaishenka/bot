import logging
from google_sheets_reader import GoogleSheetsReader
from gitlab_api import GitLabUser, are_users_valid, init_repo_for_student, rebase_repo_for_student
from tqdm import tqdm
import json
from config import MAX_RETRY, BROKEN_GROUPS

logger = logging.getLogger(__name__)


def get_gitlab_users(table_url):
    records = GoogleSheetsReader.get_all_records(table_url)
    gitlab_users = []
    for record in tqdm(records, desc="Processing records in table"):
        full_name = str(record['full_name'])
        group = str(record['group'])
        if group in BROKEN_GROUPS:
            group = '0' + group
        gitlab_username = str(record['gitlab_username']).replace('https://gitlab.com/', '')
        user = GitLabUser(gitlab_username, full_name, group)
        gitlab_users.append(user)
    return gitlab_users


def update_repos_for_students(gitlab_users: [], repo_config, consider_group_name=False, update_files=True, rebase=False):
    for user in tqdm(gitlab_users, desc="Updating repo for students"):
        for i in range(MAX_RETRY):
            try:
                init_repo_for_student(user, repo_config, consider_group_name, update_files, rebase)
                break
            except BaseException as e:
                logger.error("Error {}. Retries {}/{}".format(str(e), i + 1, MAX_RETRY))
                continue


def rebase_repos_for_students(gitlab_users: [], repo_config, consider_group_name=False):
    for user in tqdm(gitlab_users, desc="Rebasing repo for students"):
        for i in range(MAX_RETRY):
            try:
                rebase_repo_for_student(user, repo_config, consider_group_name)
                break
            except BaseException as e:
                logger.error("Error {}. Retries {}/{}".format(str(e), i + 1, MAX_RETRY))
                continue


def process_answers(config):
    try:
        table_url = config['table_url']
        config_path = config['config_path']
        update_files = config['update_files']
        consider_group_name = config['consider_group_name']
        rebase = config['rebase']
        only_rebase = config['only_rebase']
    except:
        logger.error("Invalid config! Abort.")
        return

    gitlab_users = get_gitlab_users(table_url)
    if not are_users_valid(gitlab_users):
        logger.error("Users aren't valid! Abort.")
        return

    with open(config_path, 'r') as repo_config:
        if only_rebase:
            rebase_repos_for_students(gitlab_users, json.load(repo_config), consider_group_name)
        else:
            update_repos_for_students(gitlab_users, json.load(repo_config), consider_group_name, update_files, rebase)


def process_config(config_path: str):
    with open(config_path, 'r') as config:
        process_answers(json.load(config))


def main():
    process_config('configs/config_111_122.json')


if __name__ == '__main__':
    main()
