import logging
import gitlab
import os
from tqdm import tqdm
import time

import config

logger = logging.getLogger(__name__)


def make_gitlab_api():
    return gitlab.Gitlab(config.GITLAB_URL, config.GITLAB_TOKEN)


def get_project(project_url):
    api = make_gitlab_api()
    try:
        project = api.projects.get(project_url)
        return project
    except:
        return None


def get_url_to_clone_with_token(project):
    project_url = project.http_url_to_repo.split('https://gitlab.com/')[1]
    return 'https://oauth2:{}@gitlab.com/{}'.format(config.GITLAB_TOKEN, project_url)


def get_branch(project, branch_name='master'):
    branches = project.branches.list()
    for branch in branches:
        if branch.name == branch_name:
            return branch
    return None


def get_group(group):
    api = make_gitlab_api()
    group = api.groups.get(group)
    return group


def give_access_to_project(gitlab_user, project, access_level):
    member = project.members.create({
        "user_id": gitlab_user.id,
        "project_id": project.id,
        "access_level": access_level,
    })
    return member


def add_submodules(project, branch_name, submodules, user):
    branch = get_branch(project, branch_name)
    if not branch:
        print("Branch {} doesn't exist! Can't add submodules".format(branch_name))
        logger.info('Trying to add submodules to not existing branch {}'.format(branch_name))
        return
    file_name = '.gitmodules'
    gitmodules_content = ''
    try:
        gitlab_file = project.files.get(file_path=file_name, ref=branch_name)
        gitmodules_content = gitlab_file.decode().decode('ascii')
    except:
        pass
    submodule_str = '[submodule "{}"]\n\tpath = {}\n\turl = {}\n'
    content = ''
    for submodule in submodules:
        if submodule in gitmodules_content:
            print('Submodule {} alredy in repo. Continue.'.format(submodule))
            continue
        path = submodules[submodule].get('path')
        url = submodules[submodule].get('url')
        full_url = submodules[submodule].get('full_url')
        if path is None or url is None or full_url is None:
            print("Can't init submodule. Missing path or url. Continue")
            logger.info("Can't init submodule. Missing path or url. Continue")
            continue
        submodule_project = get_project(full_url)
        if not submodule_project:
            print("Project doesn't exist. Continue")
            logger.info("Project doesn't exist. Continue")
            continue
        if not user.has_access(submodule_project):
            give_access_to_project(user.get_gitlab_user(), submodule_project, gitlab.DEVELOPER_ACCESS)

        url_to_clone = get_url_to_clone_with_token(project)
        os.system('git clone {} tmp_project'.format(url_to_clone))
        os.chdir('tmp_project')
        os.system('git pull')
        os.system('git submodule add {}'.format(submodule_project.http_url_to_repo))
        os.system('git commit -am "add submodule {}"'.format(submodule))
        os.system('git push {}'.format(url_to_clone))
        os.chdir('../')
        os.system('rm -r tmp_project')
        content += submodule_str.format(submodule, path, url)
    if not content:
        print('Submodules init finish. Continue')
        return
    try:
        gitlab_file = project.files.get(file_path=file_name, ref=branch_name)
        gitlab_file.content = content
        gitlab_file.save(branch=branch_name, commit_message='Init submodules')
    except:
        project.files.create({
            'file_path': file_name,
            'branch': branch_name,
            'content': content,
            'author_email': config.GITLAB_AUTHOR_EMAIL,
            'author_name': config.GITLAB_AUTHOR_NAME,
            'commit_message': 'Init submodules'
        })
    print('Submodules init finish. Continue')


def rebase_branch(project, branch_to_rebase_name, source_branch_name):
    print(f"Begin rebasing branch {branch_to_rebase_name}")
    branch = get_branch(project, branch_to_rebase_name)
    if not branch:
        print(f"Branch {branch_to_rebase_name} doesn't exist! Can't rebase")
        logger.info('Trying to rebase not existing branch {}'.format(branch_to_rebase_name))
        raise Exception(f"Branch {branch_to_rebase_name} doesn't exist to rebase")
    url_to_clone = get_url_to_clone_with_token(project)
    os.system('git clone {} tmp_project'.format(url_to_clone))
    os.chdir('tmp_project')
    os.system(f'git checkout {branch_to_rebase_name}')
    os.system(f'git rebase {source_branch_name}')
    os.system('git push origin --force')
    os.chdir('../')
    os.system('rm -r tmp_project')
    print(f"Rebased {branch_to_rebase_name} to {source_branch_name}. Continue")


def init_branch(project, branch_config, branch_name, config_path, update_files):
    branch = get_branch(project, branch_name)

    if not branch and branch_name != 'master':
        ref = branch_config.get('ref')
        if ref is None:
            print("Can't create branch {}. Missing 'ref' in config. Continue".format(branch_name))
            logger.error("Can't create branch {}. Missing 'ref' in config".format(branch_name))
            return None
        branch = project.branches.create({
            'branch': branch_name,
            'ref': ref
        })

    branch_files = branch_config.get('files')

    for file_name in branch_files:
        gitlab_file = None
        if branch:  # dirty hack to avoid clean repo troubles
            try:
                gitlab_file = project.files.get(file_path=file_name, ref=branch_name)
                print('File {} already exists'.format(file_name))
                if update_files:
                    print('Updating file {}'.format(file_name))
                else:
                    continue
            except:
                pass
        path_to_file = config_path + '/' + branch_name + '/' + file_name
        with open(path_to_file, 'r') as file:
            data = ''.join(file.readlines())
            if gitlab_file and update_files:
                gitlab_file.content = data
                gitlab_file.save(branch=branch_name, commit_message='Updating file {}'.format(file_name))
            else:
                project.files.create({
                    'file_path': file_name,
                    'branch': branch_name,
                    'content': data,
                    'author_email': config.GITLAB_AUTHOR_EMAIL,
                    'author_name': config.GITLAB_AUTHOR_NAME,
                    'commit_message': 'Initing repo: add file {}'.format(file_name)
                })

    branch = get_branch(project, branch_name)
    protect = branch_config.get('protect')
    if protect:
        branch.protect()

    print('Branch {} inited'.format(branch_name))
    return branch


def init_branches(project, branches, user, submodules, config_path, update_files, rebase):
    if 'master' in branches:  # We should init master first
        branch_config = branches['master']
        init_branch(project, branch_config, 'master', config_path, update_files)
        if submodules:
            add_submodules(project, 'master', submodules, user)

    for branch_name in tqdm(branches):
        if branch_name == 'master':
            continue
        branch_config = branches[branch_name]
        init_branch(project, branch_config, branch_name, config_path, update_files)
        if rebase:
            time.sleep(2)
            rebase_branch(project, branch_name, "master")


def rebase_branches(project, branches):
    for branch_name in branches:
        if branch_name == 'master':
            continue
        rebase_branch(project, branch_name, 'master')


class GitLabUser:
    def __init__(self, username: str, fullname: str, groupname: str):
        self.username = username
        self.fullname = fullname.replace(' ', '-')
        self.groupname = groupname

    def get_project_name(self):
        projectname = '{}-{}-u-{}'.format(
            self.groupname, self.fullname, self.username)
        return projectname

    def get_url_for_user(self, gitlab_group):
        url = '{}/{}/{}'.format(config.GITLAB_URL, gitlab_group, self.get_project_name())
        return url

    def is_exists(self):
        api = make_gitlab_api()
        users = api.users.list(username=self.username)
        return len(users) == 1

    def get_project(self, course_group):
        api = make_gitlab_api()
        project_name = self.get_project_name()
        projects = list(filter(lambda project: project.name == project_name,
                               course_group.projects.list(search=project_name)))

        if len(projects) > 0:
            return api.projects.get(projects[0].id)

        return None

    def get_gitlab_user(self):
        if not self.is_exists():
            return None

        api = make_gitlab_api()
        users = api.users.list(username=self.username)
        return users[0]

    def has_access(self, project):
        user = self.get_gitlab_user()

        if not user:
            return False

        members = project.members.all(all=True)

        for member in members:
            if user.id == member.id:
                return True

        return False

    def __str__(self):
        return self.username

    def __repr(self):
        return str(self)


def are_users_valid(gitlab_users: []):
    valid = True
    for user in tqdm(gitlab_users, desc="Check users exist"):
        if not user.is_exists():
            valid = False
            logger.error(f"User {user} doesn't exists!")

    return valid


def init_repo_for_student(user, repo_config, consider_group_name=False, update_files=True, rebase=False):
    api = make_gitlab_api()
    gitlab_user = user.get_gitlab_user()

    if not gitlab_user:
        logger.error("User doesn't exist")
        return "User doesn't exist"

    if repo_config.get('group_name') is None:
        logger.error('Invalid repo config!')
        return config.ERROR_CREATING_REPO

    config_path = repo_config.get('config_path')
    if config_path is None:
        logger.error('Invalid repo config!')
        return config.ERROR_CREATING_REPO

    group_name = repo_config['group_name']
    if consider_group_name:
        group_name += '/' + user.groupname
    group = get_group(group_name)

    if group is None:
        logger.error("Can't find group!")
        return config.ERROR_CREATING_REPO

    project_name = user.get_project_name()
    project = user.get_project(group)

    if project is None:
        print('Existing project not found, create new')
        project = api.projects.create({
            "name": project_name,
            "namespace_id": group.id,
        })
    else:
        print("Project already exists")
        logger.info("Project already exists: {}".format(project_name))

    if not user.has_access(project):
        print("Register project member")
        give_access_to_project(gitlab_user, project, gitlab.DEVELOPER_ACCESS)

    branches = repo_config.get('branches')
    if branches is None:
        print('Complete')
        return config.COMPLETE_MESSAGE

    submodules = repo_config.get('submodules')
    init_branches(project, branches, user, submodules, config_path, update_files, rebase)
    print('Complete')

    return config.COMPLETE_MESSAGE


def rebase_repo_for_student(user, repo_config, consider_group_name=False):
    api = make_gitlab_api()
    gitlab_user = user.get_gitlab_user()

    if not gitlab_user:
        logger.error("User doesn't exist")
        return "User doesn't exist"

    if repo_config.get('group_name') is None:
        logger.error('Invalid repo config!')
        return config.ERROR_CREATING_REPO

    config_path = repo_config.get('config_path')
    if config_path is None:
        logger.error('Invalid repo config!')
        return config.ERROR_CREATING_REPO

    group_name = repo_config['group_name']
    if consider_group_name:
        group_name += '/' + user.groupname
    group = get_group(group_name)

    if group is None:
        logger.error("Can't find group!")
        return config.ERROR_CREATING_REPO

    project = user.get_project(group)

    if project is None:
        print('Existing project not found')
        logger.error("Can't rebase repo, because existing project not found")
        return

    branches = repo_config.get('branches')
    if branches is None:
        print('Complete')
        return config.COMPLETE_MESSAGE

    rebase_branches(project, branches)

user = GitLabUser('GoncharenkoMikhail', 'Goncharenko Mikhail', '952')
import json
with open('foreigners_algo_repo_config/config.json', 'r') as config_file:
    json_config = json.load(config_file)
    init_repo_for_student(user, json_config, True, False, False)
