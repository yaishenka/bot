import json
import glob
import os
import itertools


# {
#   "config_path": "111_repo_config",
#   "group_name": "mipt_algo_111_1sem",
#   "branches": {
#     "master": {
#       "protect": true,
#       "files": ["README.md", ".gitignore", ".gitlab-ci.yml"],
#       "ref": null
#     },
#     "1A": {
#       "protect": false,
#       "files": ["1A/main.cpp", "1A/README.md"],
#       "ref": "master"
#     },
#     "1D": {
#       "protect": false,
#       "files": ["1D/main.cpp", "1D/README.md"],
#       "ref": "master"
#     },
#     "1F": {
#       "protect": false,
#       "files": ["1F/main.cpp", "1F/README.md"],
#       "ref": "master"
#     }
#   },
#   "submodules": {
#     "testing_repo": {
#       "path": "testing_repo",
#       "url": "../testing_repo",
#       "full_url": "mipt_algo_111_1sem/testing_repo"
#     }
#   }
# }


def generate_config(path_to_dir, group_name, submodules_config: {}):
    config = {'config_path': path_to_dir, 'group_name': group_name, 'submodules': submodules_config}

    branches_config = {}
    branches = [dir for dir in os.listdir(path_to_dir) if os.path.isdir(os.path.join(path_to_dir, dir))]
    for branch in branches:
        branch_config = {'files': []}
        files = set()
        if branch != 'master':
            branch_config['ref'] = 'master'
            branch_config['protect'] = False
        else:
            branch_config['ref'] = None
            branch_config['protect'] = True

        iterator = itertools.chain(glob.iglob(os.path.join(path_to_dir, branch, '**', '**'), recursive=True),
                                   glob.iglob(os.path.join(path_to_dir, branch, '**', '.**'), recursive=True))

        for file_name in iterator:
            if os.path.isdir(file_name) or '.DS_Store' in file_name:
                continue
            file_name = file_name.replace(os.path.join(path_to_dir, branch) + '/', '')
            files.add(file_name)
        branch_config['files'] = list(files)

        branches_config[branch] = branch_config

    config['branches'] = branches_config

    return config


def main():
    config_path = '111_cpp_repo_config'
    submodules_config = {
        "testing_repo": {
          "path": "testing_repo",
          "url": "../testing_repo",
          "full_url": "mipt_cpp_111/testing_repo"
        }
    }
    dictionary = generate_config(config_path, 'mipt_cpp_111', submodules_config)

    with open(os.path.join(config_path, 'config.json'), 'w') as config_file:
        json.dump(dictionary, config_file, indent=4)



if __name__ == '__main__':
    main()
